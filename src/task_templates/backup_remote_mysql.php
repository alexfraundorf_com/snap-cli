#!/usr/bin/env php
<?php
/**
 * Trigger remote MySql database backup and pull a copy back to the local machine.
 * 
 * This file must be executable.
 * 
 * Template Data: 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @package Snap\Cli
 * @version 10/17/2016
 * 
 */

// Note: This file assumes (and requires) that the remote server has a 
// mysql config file on it containing the username and password for the target
// database.

// name of host/alias for the remote server key (Host value in .ssh/config file)
$host_name = '!!!HOST NAME OF REMOTE MACHINE!!!'; 

// path to directory on local machine to store backups
$local_backup_directory = '/PATH/TO/STORE/BACKUPS/ON/LOCAL/MACHINE';

// name of mysql config file on server
$mysql_config_file = '.mysql.conf';

// this version will keep only one compressed copy of the database on the server
$backup_file_name = 'db_backup.sql.gz';
// this version will keep a daily compressed copy of the database on the server 
// note: this can eat up server space very quickly
//$backup_file_name = 'db_backup_' . date('Y-m-d') . '.sql.gz';


// check required variables
if(!$host_name || $host_name === '!!!HOST NAME OF REMOTE MACHINE!!!') {
    throw new \InvalidArgumentException('You need to set the value of '
            . '$host_name in ' . __FILE__);
}
if(!is_dir($local_backup_directory)) {
    throw new \InvalidArgumentException('The local backup directory (' 
            . $local_backup_directory . ') does not appear to exist. '
            . 'Please create it.');
}
if(!$mysql_config_file) {
    throw new \InvalidArgumentException('You need to set a value for '
            . '$mysql_config_file in ' . __FILE__);
}
if(!$backup_file_name) {
    throw new \InvalidArgumentException('You need to set a value for '
            . '$backup_file_name in ' . __FILE__);
}

// build the command
$backup_command = 'ssh ' . $host_name . ' \'mysqldump --defaults-extra-file="' 
        . $mysql_config_file . '" --all-databases | gzip > ' 
        . $backup_file_name . '; exit;\'';
// execute the backup command on the remote server
$backup_output = [];
exec($backup_command, $backup_output);
// do something with the $backup_output array if you want

// copy the backup to the local machine
$copy_command = 'scp ' . $host_name . ':~/' . $backup_file_name . ' ' 
        . $local_backup_directory . '/' . $backup_file_name . '; exit;';
// execute the remote copy command 
$copy_output;
exec($copy_command, $copy_output);
// do something with the $copy_output array if you want
