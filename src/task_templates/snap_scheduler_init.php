#!/usr/bin/env php
<?php
/**
 * Snap Scheduler Initializer - Checks of Scheduler is running and starts it if it isn't.
 * 
 * Template Data: 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @package Snap\Cli
 * @version 10/20/2016
 * 
 */

// set the path to the scheduler script
$scheduler_path = __DIR__ . '/snap_scheduler.php';

require('Scheduler.php');

// check if the scheduler is running, and start it if it isn't
if(!\Snap\Scheduler\Scheduler::isRunning()) {
    exec($scheduler_path);
}
