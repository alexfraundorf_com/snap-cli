#!/usr/bin/env php
<?php
/**
 * Recursively remove old files and empty directories in directory
 * 
 * Template Data: 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @package Snap\Cli
 * @version 10/18/2016
 * 
 */

// remove all files over x days old
$max_age_of_file_days = 30;

// path to the directory to clean
$directory_path = '/PATH/TO/DIRECTORY/TO/CLEAN';

// action to take ("trash" or "delete")
// if "trash" the old files will be placed in the trash/recyle can so they can 
// still be manually retrieved
// if "delete" they will be permenantly deleted (dangerous)
$action = 'trash';

//$path_to_trash = '/home/USERNAME/.local/share/Trash'; // linux
//$path_to_trash = '/home/USERNAME/.Trash'; // mac
//$path_to_trash = '\$Recycle.Bin\%SID%'; // windows
$path_to_trash = '/PATH/TO/TRASH/CAN';





/**
 * Check if a directory is empty or not.
 * 
 * @param string $path_to_directory
 * @return boolean
 */
function is_directory_empty($path_to_directory) {
    if(count(glob($path_to_directory . '/*')) === 0) {
        return true;
    }
    return false;
}


if(!is_dir($directory_path)) {
    throw new \ErrorException('The value for $directory_path is not a directory. Please correct.');
}
if($action === 'trash' && !is_dir($path_to_trash)) {
    throw new \ErrorException('The value for $path_to_trash is not a directory. Please correct.');
}
// initialize
$removed = [];
// recursively get the directory structure
$Directory = new \RecursiveDirectoryIterator($directory_path);
$Iterator = new \RecursiveIteratorIterator($Directory);

// loop through and check the files
foreach ($Iterator as $SplFileInfo) {
    if($SplFileInfo->isFile()) {
        
        // check the time (in seconds) since the file was last changed
        $file_age = time() - $SplFileInfo->getCTime();
        $max_age_seconds = $max_age_of_file_days * 24 * 60 * 60;
        
        if($file_age > $max_age_seconds) {
            
            //echo $SplFileInfo->getPathname() . ' ' . $file_age . PHP_EOL; continue; // for testing
            
            // move the file to trash
            if($action === 'trash') {
                if(rename($SplFileInfo->getPathname(), $path_to_trash . '/' 
                        . $SplFileInfo->getFilename())) {
                    $removed[] = $SplFileInfo->getPathname();
                }
                else {
                    throw new \ErrorException('Failed to move file (' 
                            . $SplFileInfo->getPathname() . ') to trash.');
                }
            }
            
            // delete the file
            elseif($action === 'delete') {
                if(unlink($SplFileInfo->getPathname())) {
                    $removed[] = $SplFileInfo->getPathname();
                }
                else {
                    throw new \ErrorException('Failed to delete file (' 
                            . $SplFileInfo->getPathname());
                }
            }
            
            else {
                throw new \UnexpectedValueException('Unsupported value for $action');
            }
        }
    }

}


// files have been removed - now delete empty directories
foreach ($Iterator as $SplFileInfo) {
    
    if($SplFileInfo->isDir() && is_directory_empty($SplFileInfo->getPathname())) {
        
        if(rmdir(rtrim($SplFileInfo->getPathname(), '/.'))) {
            $removed[] = $SplFileInfo->getPathname();
        }
        else {
            throw new \ErrorException('Failed to delete directory (' 
                    . $SplFileInfo->getPathname());
        }
        
    }
        
}


// do something with the array of removed files/directories
//var_dump($removed);