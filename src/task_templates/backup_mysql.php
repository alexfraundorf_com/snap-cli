#!/usr/bin/env php
<?php
/**
 * Back Up All MySql Databases
 * 
 * This file must be placed on the machine with the database to back up, and 
 * must be executable.
 * 
 * Template Data: 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @package Snap\Cli
 * @version 10/15/2016
 * 
 */

// Note: the file .my.cnf holds the mysql database username and password so it is kept as secure as possible
// IMPORTANT: MAKE SURE THAT .my.cnf IS NOT IN THE public_html directory!!!!

// IMPORTANT: MAKE SURE THAT THE DATABASE BACKUP IS NOT STORED IN THE public_html DIRECTORY!!!


// this version will keep only one compressed copy of the database on the server
$command = 'mysqldump --defaults-extra-file=".mysql.conf" --all-databases | gzip > db_backup.sql.gz';

// this version will keep a daily compressed copy of the database on the server - this can eat up server space very quickly
//$command = 'mysqldump --defaults-extra-file=".mysql.conf" --all-databases | gzip > db_backup_' . date('Y-m-d') . '.sql.gz';

// execute the command
$ouput = [];
exec($command, $output);

// do something with the $output array if you want