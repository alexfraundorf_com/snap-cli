#!/usr/bin/env php
<?php
/**
 * Snap Scheduler Example
 * 
 * Template Data: 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @package Snap\Cli
 * @version 10/20/2016
 * 
 */

require('Scheduler.php');

// create the scheduler object
$Scheduler = new \Snap\Scheduler\Scheduler(__DIR__, __FILE__);
  
// add a super annoying five minute task
//$Scheduler->addFiveMinuteTask('create_note.php');
      
// schedule a daily database backup
// note: this assumes the existance of a database backup script in the same directory!
//$Scheduler->addDailyTask('backup_remote_mysql.php');

// run
$Scheduler->run();
