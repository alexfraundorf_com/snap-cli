#!/usr/bin/env php
<?php
/* 
 * A super annoying script to put a message file on your desktop when ever it is run.
 * 
 * Template Data: 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @package Snap\Cli
 * @version 10/20/2016
 */

$path_to_desktop = '/home/USERNAME/Desktop';


$messages = [
    'You are amazing!',
    'You look great!',
    'You are loved!',
    'You are the best PHP developer!', 
    'You smell good!',
];

// pick a random message
$key = array_rand($messages);

// put the note on the desktop
file_put_contents($path_to_desktop . '/Message_' . time() . '.txt', $messages[$key]);
