<?php
/*
 * Snap CLI is an interactive program to make repeditive server tasks a snap!
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\Cli
 * @version 10/20/2016 0.1.0
 * @since 10/10/2016  0.1.0
 * @license MIT - https://opensource.org/licenses/MIT
 */
namespace Snap\Cli;

// include the abstract file so the autoloader doesn't have to
include_once('SnapCliAbstract.php');

class SnapCli extends SnapCliAbstract {
    
    protected $app_title = 'Snap CLI';
    
    protected $app_version = '0.1.0';
    
    protected $menu = [
        'Create Key Pair' => 'createKey',
        'Delete Key Pair' => 'deleteKey',
        'List Key Pairs' => 'listKeys',
        'Copy Key to Remote Server' . EOL . '        (to allow ssh via key pair)' => 'copyKeyToRemoteServer',
        //'Copy SnapCli Program to Remote Server' => 'copyProgram', //TODO
        //'Create a CLI Task' => 'createTask',
        'Create MySQL Backup Task (Local)' => 'createMysqlBackupTaskLocal',
        'Create MySQL Backup Task (Remote)' => 'createMysqlBackupTaskRemote',
        'Create a New Scheduler (Automated Tasks)' => 'createSchedulerTask',
        'Create a New File Cleaner Task' => 'createFileCleanerTask',
    ];
    
    
    protected function createFileCleanerTask() {
        $this->displayHeader('Create a File Cleaner');
        $destination_directory = $this->prompt('What is the absolute file path '
                . 'to the directory we should place the scripts in? '
                . '(default: ' . $this->home_dir_path . '/bin)' . EOL 
                . 'Important: Make sure this is not a web accessible directory!' . EOL, 
                $this->home_dir_path . '/bin');
        // check the destination directory
        if(!is_dir($destination_directory)) {
            throw new \InvalidArgumentException('The directory (' 
                    . $destination_directory . ') does not exist on this machine.'
                    . EOL . 'Please created it and try again.');
        }
        // copy the files
        $task_script_path1 = __DIR__ . '/task_templates/file_cleanup.php';
        $destination_path1 = $destination_directory . '/file_cleanup.php';
        copy($task_script_path1, $destination_path1);
        $this->successMsg('The file cleaner task has been created at ' 
                . $destination_path1);
        // make the task executable
        chmod($destination_path1, 0755);
        $this->successMsg('The file cleaner task has been made executable.');        
        $this->out(EOL . 'IMPORTANT: You will need to edit the created file and '
                . 'set the directory path you would like to clean, and the '
                . 'path to the trash/recycle directory before using it.' . EOL);
        $this->promptPressEnterToContinue();      
    }
    
    protected function createSchedulerTask() {
        $this->displayHeader('Create a New Snap Scheduler (Automated Tasks)');
        $destination_directory = $this->prompt('What is the absolute file path '
                . 'to the directory we should place the scripts in? '
                . '(default: ' . $this->home_dir_path . '/bin)' . EOL 
                . 'Important: Make sure this is not a web accessible directory!' . EOL, 
                $this->home_dir_path . '/bin');
        // check the destination directory
        if(!is_dir($destination_directory)) {
            throw new \InvalidArgumentException('The directory (' 
                    . $destination_directory . ') does not exist on this machine.'
                    . EOL . 'Please created it and try again.');
        }
        // copy the files
        $task_script_path1 = __DIR__ . '/task_templates/snap_scheduler.php';
        $destination_path1 = $destination_directory . '/snap_scheduler.php';
        copy($task_script_path1, $destination_path1);
        $this->successMsg('The scheduler task has been created at ' 
                . $destination_path1);
        // make the task executable
        chmod($destination_path1, 0755);
        $this->successMsg('The scheduler task has been made executable.'); 
        $task_script_path2 = __DIR__ . '/task_templates/snap_scheduler_init.php';
        $destination_path2 = $destination_directory . '/snap_scheduler_init.php';
        copy($task_script_path2, $destination_path2);
        $this->successMsg('The scheduler init file has been created at ' 
                . $destination_path2);
        // make the task executable
        chmod($destination_path2, 0755);
        $this->successMsg('The scheduler init file has been made executable.'); 
        // copy the scheduler classes
        copy(__DIR__ . '/Scheduler/Scheduler.php', 
                $destination_directory . '/Scheduler.php');
        copy(__DIR__ . '/Scheduler/SchedulerInterface.php', 
                $destination_directory . '/SchedulerInterface.php');
        // copy the create note task
        copy(__DIR__ . '/task_templates/create_note.php', 
		$destination_directory . '/create_note.php');
		chmod($destination_directory . '/create_note.php', 0755);
        // cron/scheduler instructions
        if($this->host_operating_system === 'posix') {
            $this->out($this->addCronJob($destination_path2));
        }
        elseif($this->host_operating_system === 'windows') {
            $this->out($this->addWindowsSchedulerJob($destination_path2));
        }
        $this->promptPressEnterToContinue();      
    
    }
    
    
    protected function addCronJob($path_to_file) {
        $command = 'crontab -l | { cat; echo "*/10 * * * * ' . $path_to_file . '"; } | crontab -';
        exec($command);
        $msg = 'Your file has been added as a new cron job.' . EOL . 'You can '
                . 'view the existing jobs by entering "crontab -e" in a terminal.';
        return $msg;
    }
    
    protected function addWindowsSchedulerJob($path_to_file) {
        $command = 'schtasks /create /sc minute /mo 10 /tn "Snap Scheduler" /tr "php ' 
		    . str_replace('\C:', 'C:', str_replace('/', '\\', $path_to_file)) . '"';
        exec($command);
        $msg = 'Your file has been added as to Windows scheduler.' . EOL . 'You can '
                . 'view the existing jobs by entering "schtasks /query /v /fo LIST" in a terminal.';
        return $msg;
    }

    
    protected function copyProgram() {
        $this->displayHeader('Copy SnapCli Program to Remote Server');
        $this->out('What remote machine should we copy to?' . EOL);
        $key_name = $this->displayKeyMenuAndGetKeyName();
        // make sure the phar archive is present
        $phar_name = 'snap_cli_' . $this->app_version . '.phar';
        $phar_path = __DIR__ . '/phars/' . $phar_name;
        if(!is_file($phar_path)) {
            throw new \ErrorException('There is no phar file located at ' . $phar_path);
        }
        $output = [];
        exec('scp ' . $phar_path . ' ' . $key_name . ':~/', $output);
        if($output) {
            // if there is output, there was an error
            $this->echoArray($output);
        }
        else {
            $this->successMsg('A copy of SnapCli (' . $phar_name 
                    . ') has been copied to the ~ (home) directory of ' . $key_name);
        }
        // make the file executable
        $this->remoteExec($key_name, 'chmod +x ' . $phar_name);
        $this->successMsg('The program has been made executable.' . EOL 
                . 'To use, SSH into the remote machine from a new terminal window:' 
                . EOL . 'ssh ' . $key_name . EOL
                . 'and then enter' . EOL . './' . $phar_name . EOL 
                . 'Suggestion: Move executable files to the bin directory.' . EOL);
        $this->promptPressEnterToContinue(); 
    }
    
    
    protected function createMysqlBackupTaskLocal() {
        $this->displayHeader('Create MySql Backup Task for this Machine');
        $this->out('Note: This task will be created on the local machine.' . EOL 
                . 'If you want to create this task on a remote machine' . EOL 
                . 'please SSH into that machine and run SnapCli on it locally.' . EOL);
        /*
        $destination = $this->prompt('Where are we creating this task? (local/remote)');
        if($destination === 'remote') {
            $key_name = $this->displayKeyMenuAndGetKeyName();
        }
        elseif($destination === 'local') {
            $key_name = 'local';
        }
        else {
            $this->errorMsg('Invalid response. Response must be "local" or "remote".');
        }
         * 
         */
        $destination_directory = $this->prompt('What is the absolute file path '
                . 'to the directory we should place the scripts in? '
                . '(default: ' . $this->home_dir_path . ')' . EOL 
                . 'Important: Make sure this is not a web accessible directory!' . EOL, 
                $this->home_dir_path);
        $db_user = $this->prompt('What is the MySql username?');
        $db_password = $this->prompt('What is the MySql password?');
        // check the destination directory
        if(!is_dir($destination_directory)) {
            throw new \InvalidArgumentException('The directory (' 
                    . $destination_directory . ') does not exist on this machine.'
                    . EOL . 'Please created it and try again.');
        }
        // create the config file output
        $config_output = '[client]' . EOL . 'user="' . $db_user . '"' . EOL 
                . 'password="' . $db_password . '"' . EOL;
        // write the config file and backup script
        $task_script_path = __DIR__ . '/task_templates/backup_mysql.php';
        //if($destination === 'local') {
            // create the mysql config file
            $config_destination_path = $destination_directory . '/.mysql.conf';
            file_put_contents($config_destination_path, $config_output);
            $this->successMsg('The MySql configuration file has been created at ' 
                    . $config_destination_path);
            // copy the backup task
            $backup_destination_path = $destination_directory . '/backup_mysql.php';
            copy($task_script_path, $backup_destination_path);
            $this->successMsg('The MySql backup task has been created at ' 
                    . $backup_destination_path);
            // make the task executable
            chmod($backup_destination_path, 0755);
            $this->successMsg('The MySql backup task has been made executable.' 
                    . EOL . 'You can run it by entering' . EOL 
                    . 'cd ' . $destination_directory . EOL . './backup_mysql.php');
        /*
        }
        elseif($destination === 'remote') {
            // not working yet - need to debug
            // create the mysql config temp file
            $temp_file_path = __DIR__ . '/task_templates/temp_conf';
            file_put_contents($temp_file_path, $config_output);
            // copy the mysql config file to the remote machine
            $this->scp($temp_file_path, $key_name . ':' . $destination_directory . '/.mysql.conf');
            // delete the temp config file
            unlink($temp_file_path);
            // copy the back up task to the remote machine
            $destination_path = $key_name . ':' .$destination_directory . '/backup_mysql.php';
            $this->scp($task_script_path, $destination_path);
            // make the script executable
            $this->remoteExec($key_name, 'chmod +x ' . $destination_directory . '/backup_mysql.php');
        }
        */
        
        $this->promptPressEnterToContinue();    
    }
    
    
    protected function createMysqlBackupTaskRemote() {
        $this->displayHeader('Create MySql Backup Task for a Remote Machine');
        $this->out('Note: This task will be created on the local machine.' . EOL 
                . 'It will connect to the remote machine, create a backup and ' . EOL 
                . 'download a copy of the backup to this machine.' . EOL);
        $destination_directory = $this->prompt('What is the absolute file path '
                . 'to the directory we should place the scripts in? '
                . '(default: ' . $this->home_dir_path . '/bin)' . EOL 
                . 'Important: Make sure this is not a web accessible directory!' . EOL, 
                $this->home_dir_path . '/bin');
        // check the destination directory
        if(!is_dir($destination_directory)) {
            throw new \InvalidArgumentException('The directory (' 
                    . $destination_directory . ') does not exist on this machine.'
                    . EOL . 'Please created it and try again.');
        }
        // copy the backup task
        $task_script_path = __DIR__ . '/task_templates/backup_remote_mysql.php';
        $backup_destination_path = $destination_directory . '/backup_remote_mysql.php';
        copy($task_script_path, $backup_destination_path);
        $this->successMsg('The MySql remote backup task has been created at ' 
                . $backup_destination_path);
        // make the task executable
        chmod($backup_destination_path, 0755);
        $this->successMsg('The MySql remote backup task has been made executable.' 
                . EOL . 'You can run it by entering' . EOL 
                . 'cd ' . $destination_directory . EOL . './backup_remote_mysql.php');
        $this->out(EOL . 'IMPORTANT: You will need to edit the created file and '
                . 'set the host name of the server you wish to connect to and '
                . 'the backup directory path on the local machine before using it.' 
                . EOL . EOL . 'Also, make sure that your remote machine has a MySql '
                . 'config file. An example can be found in '
                . 'task_templates/.mysql.conf.example' . EOL);
        $this->promptPressEnterToContinue();    
    }

    
    protected function copyKeyToRemoteServer() {
        $this->displayHeader('Copy Key to Remote Server');
        $this->out('Note: Mac users will need to install the ssh-copy-id program.' 
                . EOL . 'Google "ssh-copy-id for mac" for instructions.' . EOL 
                . EOL . 'What remote machine should we copy a key to?' . EOL);
        $key_name = $this->displayKeyMenuAndGetKeyName();
        $output = [];
        exec('ssh-copy-id -i ~/.ssh/' . $key_name . '.pub ' . $key_name, $output);
        $this->echoArray($output);
        $this->promptPressEnterToContinue(); 
    }
    
    
    protected function deleteKey() {
        $this->displayHeader('Delete Key Pair');
        $key_name = $this->displayKeyMenuAndGetKeyName();
        $this->deleteKeyPair($key_name);
        $this->successMsg('The key pair (' . $key_name . ') has been deleted, '
                . 'but if it was entered in your ssh config file you may '
                . 'want to remove it manually (to keep things tidy).');
        $this->promptPressEnterToContinue();
    }
    
    protected function displayKeyMenuAndGetKeyName() {
        // get array of keys
        $keys = $this->getArrayofKeys();
        foreach($keys as $number => $name) {
            echo '    ' . $number . ' - ' . $name . EOL;
        }
        $key_number = $this->prompt('Enter the key pair to delete or "c" to cancel:');
        if($key_number === 'c') {
            return;
        }
        if(isset($keys[$key_number])) {
            return (string) $keys[$key_number];
        }
        else {
            throw new \OutOfBoundsException('Invalid key number: ' . $key_number);
        }
    }
    
    
    protected function listKeys($display_prompt = true) {
        echo EOL . 'Existing Key Pairs:' . EOL . EOL;
        // get array of keys
        $keys = $this->getArrayofKeys();
        foreach($keys as $number => $name) {
            echo '    ' . $number . ' - ' . $name . EOL;
        }
        if($display_prompt) {
            $this->promptPressEnterToContinue();
        }
    }
    
    
    protected function getArrayofKeys() {
        $keys = [];
        $i = 1;
        foreach (new \DirectoryIterator($this->home_dir_path . '/.ssh') as $fileInfo) {
            if($fileInfo->isDot()) {
                continue;
            }
            if(strpos($fileInfo->getFilename(), '.pub')) {
                $keys[$i] = (string) str_replace('.pub', '', $fileInfo->getFilename());
                $i++;
            }
        } 
        return (array) $keys;
    }
    
    
    protected function createKey() {
        $this->clearScreen();
        $this->displayHeader('Create a New Public/Private Key Pair');
        $key_name = $this->prompt('Please enter a name for your key (no spaces):');
        // check if key exists
        if($this->doesSshKeyExist($key_name)) {
            $overwrite = $this->prompt('The key pair (' . $key_name 
                    . ') already exists. Overwrite (y/n)?');
            if(strtolower($overwrite) === 'y') {
                $this->deleteKeyPair($key_name);
            }
            else {
                $this->out(EOL . 'Key creation aborted.' . EOL);
                $this->promptPressEnterToContinue();
                return;
            }
        }
        // create the key
        $output = [];
        exec('ssh-keygen -f ~/.ssh/' . $key_name . ' -C "' . $key_name . '"', $output); 
        // loop through and display the output
        $this->echoArray($output);
        $this->successMsg('Your key pair has been created.');
        // see if we should append key to .ssh/config
        $do_ssh_config = $this->prompt('Append this key to .ssh/config? (y/n)');
        if(strtolower($do_ssh_config) === 'y') {
            $this->appendKeyToSshConfig($key_name);
        }
        $this->promptPressEnterToContinue();
    }
    
    
    protected function appendKeyToSshConfig($key_name) {
        $ssh_config_file_path = $this->home_dir_path . '/.ssh/config';
        $hostname = $this->prompt('What is the URL (no leading https://www) or '
                . 'IP address of the server?');
        $port = $this->prompt('What is the server\'s SSH port? (default: 22)', 22);
        $user = $this->prompt('What is the SSH username?');
        // build the output
        $output = EOL . '# ssh config entry for ' . $key_name 
                . ' created by SnapCli on ' . date('Y-m-d') . EOL 
                . 'Host ' . $key_name . EOL
                . 'Hostname ' . $hostname . EOL
                . 'Port ' . $port . EOL
                . 'User ' . $user . EOL
                . 'IdentityFile ' . $this->home_dir_path . '/.ssh/' . $key_name 
                . EOL; 
        file_put_contents($ssh_config_file_path, $output, FILE_APPEND);
        $this->successMsg('Your key has been added to the ssh config file at ' 
                . $ssh_config_file_path);
    }
    
    
    /*
    protected function createTask() {
        echo 'create task';
        $this->promptPressEnterToContinue();
    }
    */
    
    
    protected function help() {
        $this->displayHeader('Help Page');
        
        
        $this->promptPressEnterToContinue();
    }
    
    protected function about() {
        $this->displayHeader('About ' . $this->app_title);
        $this->out('This program is a tool to automate and abstract common '
                . 'server tasks.' . EOL . EOL 
                . 'Copyright 2016 Alex Fraundorf and/or Snap Programming and '
                . 'Development LLC' . EOL . 'All rights reserved worldwide.' 
                . EOL . EOL . 'Licensed under MIT: https://opensource.org/licenses/MIT'
                . EOL . EOL . 'Contact Info: AlexFraundorf.com or SnapProgramming.com' . EOL);
        $this->promptPressEnterToContinue();
    }
    
    protected function doesSshKeyExist($name) {
        if(file_exists($this->home_dir_path . '/.ssh/' . $name) 
                || file_exists($this->home_dir_path . '/.ssh/' . $name . '.pub')) {
            return true;
        }
        return false;
    }
    
    protected function deleteKeyPair($name) {
        if(!$this->doesSshKeyExist($name)) {
            return;
        }
        // delete private key
        $private_key_path = $this->home_dir_path . '/.ssh/' . $name;
        if(is_file($private_key_path)) {
            unlink($private_key_path);
        }
        // delete public key
        $public_key_path = $this->home_dir_path . '/.ssh/' . $name . '.pub';
        if(is_file($public_key_path)) {
            unlink($public_key_path);
        }
    }
    
    
    protected function scp($source_path, $destination_path) {
        if($this->host_operating_system === 'posix') {
            exec('scp ' . $source_path . ' ' . $destination_path);
        }
        else {
            throw new \ErrorException('Unsupported host OS: ' . $this->host_operating_system);
        }
    }
    
    protected function remoteExec($key_name, $command) {
        if($this->host_operating_system === 'posix') {
            // run the command and then terminate the ssh session
            exec('ssh ' . $key_name . ' ' . $command . '; exit;');
        }
        else {
            throw new \ErrorException('Unsupported host OS: ' . $this->host_operating_system);
        }
    }
    
    
}
