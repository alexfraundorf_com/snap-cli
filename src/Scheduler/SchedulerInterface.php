<?php
/**
 * Interface to type hint against to facilitate future use of mock objects in testing
 *
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\TaskManager
 * @version 10/20/2016 0.1.0
 * @since 06/15/2016  0.1.0
 * @license MIT - https://opensource.org/licenses/MIT
 */
namespace Snap\Scheduler;

interface SchedulerInterface {
    
    
    /**
     * Add a task to be run every minute.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     */
    public function addEveryMinuteTask($task);
    
    
    /**
     * Add a task to be run every five minutes.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     */    
    public function addFiveMinuteTask($task);
    
    
    /**
     * Add a task to be run every ten minutes.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     */    
    public function addTenMinuteTask($task);
    
    
    /**
     * Add a task to be run every fifteen minutes.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     */    
    public function addFifteenMinuteTask($task);
    
    
    /**
     * Add a task to be run every thirty minutes.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     */    
    public function addThirtyMinuteTask($task);
    
    
    /**
     * Add a task to be run every hour.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     */    
    public function addHourlyTask($task);
    
    
    /**
     * Add a task to be run every day.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     */       
    public function addDailyTask($task);
    
    
    /**
     * Add a task to be run every week.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     */    
    public function addWeeklyTask($task);
    
    
    /**
     * Add a task to be run every month.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     */    
    public function addMonthlyTask($task);
        
    
    /**
     * Add a task to be run every time the object is created.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     */    
    public function addStartUpTask($task);
    
    
    /**
     * Start an infinite loop that runs the tasks.
     */
    public function run();
    
        
    /**
     * Set the data file path.
     * 
     * @param string $file_path
     * @return \Snap\Scheduler\Scheduler
     */
    public function setDataFilePath($file_path);
    
    
    /**
     * Return the data file path.
     * 
     * @return string
     */
    public function getDataFilePath();
    

    /**
     * Set the log directory.
     * 
     * @param string $directory_path
     * @return \Snap\Scheduler\Scheduler
     */
    public function setLogDirectory($directory_path);
    
    
    /**
     * Return the log directory.
     * 
     * @return string
     */
    public function getLogDirectory();
    
    
    /**
     * Destructor - log and attempt to respawn an instance.
     */
    public function __destruct();
    
    
    /**
     * See if the scheduler is running or not.
     * 
     * @return boolean
     */
    public static function isRunning();   
    
    
}
