<?php
/**
 * Task Scheduler
 *
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\TaskManager
 * @version 10/20/2016 0.1.0
 * @since 06/15/2016  0.1.0
 * @license MIT - https://opensource.org/licenses/MIT
 */
namespace Snap\Scheduler;

// manually include the interface so the autoloader doesn't need to be called
include_once('SchedulerInterface.php');

class Scheduler implements SchedulerInterface {
    
    /**
     *
     * @var bool while true the program will loop (run)
     */
    protected $loop = true;
    
    /**
     *
     * @var string absolute path to the file running the scheduler (ie: snap_scheduler.php)
     */
    protected $calling_file_path;
    
    /**
     *
     * @var int the process id of the running program
     */
    protected $process_id;
    
    /**
     *
     * @var string absolute path to the calling file's directory
     */
    protected $directory_path;
    
    /**
     *
     * @var string absolute path to data file
     */
    protected $path_to_data_file;
    
    /**
     *
     * @var string absolute path to log directory
     */
    protected $path_to_log_directory;

    /**
     *
     * @var string absolute path to check for halt flag. If the file exists, the loop will halt.
     */
    protected $path_to_halt_flag;
    
    /**
     *
     * @var array of tasks to run at start up
     */
    protected $start_up_tasks = [];
    
    /**
     *
     * @var array of tasks to run every month
     */
    protected $monthly_tasks = [];
    
    /**
     *
     * @var int timestamp of last monthly task run time
     */
    protected $monthly_task_last_run_time = 0;
    
    /**
     *
     * @var array of tasks to run every week
     */
    protected $weekly_tasks = [];
    
    /**
     *
     * @var int timestamp of last weekly task run time
     */
    protected $weekly_task_last_run_time = 0;
    
    /**
     *
     * @var array of tasks to run every day
     */
    protected $daily_tasks = [];
    
    /**
     *
     * @var int timestamp of last daily task run time
     */
    protected $daily_task_last_run_time = 0;
    
    /**
     *
     * @var array of tasks to run hour 
     */
    protected $hourly_tasks = [];
    
    /**
     *
     * @var int timestamp of last hourly task run time
     */
    protected $hourly_task_last_run_time = 0;
    
    /**
     *
     * @var array of tasks to run every 30 minutes
     */
    protected $thirty_minute_tasks = [];
    
    /**
     *
     * @var int timestamp of last 30 minute task run time
     */
    protected $thirty_minute_task_last_run_time = 0;
    
    /**
     *
     * @var array of tasks to run every 15 minutes
     */
    protected $fifteen_minute_tasks = [];
    
    /**
     *
     * @var int timestamp of last 15 minute task run time
     */
    protected $fifteen_minute_task_last_run_time = 0;
    
    /**
     *
     * @var array of tasks to run every 10 minutes
     */
    protected $ten_minute_tasks = [];
    
    /**
     *
     * @var int timestamp of last 10 minute task run time
     */
    protected $ten_minute_task_last_run_time = 0;
    
    /**
     *
     * @var array of tasks to run every 5 minutes
     */
    protected $five_minute_tasks = [];
    
    /**
     *
     * @var int timestamp of last 5 minute task run time
     */
    protected $five_minute_task_last_run_time = 0;
    
    /**
     *
     * @var array of tasks to run every minute
     */
    protected $every_minute_tasks = [];
    
    /**
     *
     * @var int timestamp of last every minute task run time
     */
    protected $every_minute_task_last_run_time = 0;
    
    
    /**
     * Constructor
     * 
     * @param string $directory_of_calling_file ie: /home/user/bin (use __DIR__ in calling file)
     * @param string $path_of_calling_file ie: /home/user/bin/snap_scheduler.php (use __FILE__ in calling file)
     * @version 0.1.0 10/20/2016
     * @since 0.1.0 06/15/2016
     */
    public function __construct($directory_of_calling_file, $path_of_calling_file) {
        // validate
        if(!is_dir($directory_of_calling_file)) {
            throw new \InvalidArgumentException('The provided value for '
                    . '$directory_of_calling_file does not appear to be a valid path.');
        }
        if(!is_file($path_of_calling_file)) {
            throw new \InvalidArgumentException('Could not locate calling file at '
                    . 'path: ' . $path_of_calling_file);
        }
        // set the file path to the script that is implementing this object
        $this->calling_file_path = (string) $path_of_calling_file;
        // set the process id
        $this->process_id = getmypid();
        // set up this file's directory
        $this->directory_path = $directory_of_calling_file . '/snap_scheduler';
        // create the directory if it doesn't exist
        if(!is_dir($this->directory_path)) {
            mkdir($this->directory_path, 0777, true);
        }
        // set the data file path
        $path_to_data_file = $this->directory_path . '/data.json';
        $this->setDataFilePath($path_to_data_file);
        // load and import the data file if one exists
        if(is_readable($path_to_data_file)) {
            $data_array = $this->loadDataFromFile($path_to_data_file);
            $this->importData($data_array);
        }
        // set up the log directory
        $log_directory_path = $this->directory_path . '/logs';
        $this->setLogDirectory($log_directory_path);
        // set the flag file
        $this->setFlagFile();
        // set halt flag path
        $this->path_to_halt_flag = $this->directory_path . '/halt';
        // log
        $this->log('--- ' . time() . ' Object (' . __CLASS__ . ' - Process ID: ' . $this->process_id 
                . ') constructor called from file (' . $path_of_calling_file . '). ---' . PHP_EOL);
        // Setup CTRL+C and System kill message handler
		if(function_exists('pcntl_signal') {
			// The only signal that cannot be caught is the SIGKILL (very hard kill)
			declare(ticks = 1); // Required else it won't work.
			// System kill (Unhappy Termination)
			pcntl_signal(SIGTERM, function() {
				$this->log(time() . ' System kill signal (SIGTERM) received on process id: ' 
						. $this->process_id . PHP_EOL);
				$this->__destruct();
			}); 
			// CTRL+C (Happy Termination)
			pcntl_signal(SIGINT, function() {
				$this->log(time() . ' System interupt signal (SIGINT) (Ctrl+C) received on process id: ' 
						. $this->process_id . PHP_EOL);
				$this->__destruct();
			}); 
		}
        // run start up tasks
        if(!empty($this->start_up_tasks)) {
            foreach($this->start_up_tasks as $task) {
                exec($task);
            }
        }
    }
    
    
    /**
     * Add a task to be run every minute.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */
    public function addEveryMinuteTask($task) {
        $this->every_minute_tasks[] = $task;
        return $this;
    }
    
    
    /**
     * Add a task to be run every five minutes.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */    
    public function addFiveMinuteTask($task) {
        $this->five_minute_tasks[] = $task;
        return $this;
    }
    
    
    /**
     * Add a task to be run every ten minutes.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */    
    public function addTenMinuteTask($task) {
        $this->ten_minute_tasks[] = $task;
        return $this;
    }
    
    
    /**
     * Add a task to be run every fifteen minutes.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */    
    public function addFifteenMinuteTask($task) {
        $this->fifteen_minute_tasks[] = $task;
        return $this;
    }
    
    
    /**
     * Add a task to be run every thirty minutes.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */    
    public function addThirtyMinuteTask($task) {
        $this->thirty_minute_tasks[] = $task;
        return $this;
    }
    
    
    /**
     * Add a task to be run every hour.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */    
    public function addHourlyTask($task) {
        $this->hourly_tasks[] = $task;
        return $this;
    }
    
    
    /**
     * Add a task to be run every day.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */       
    public function addDailyTask($task) {
        $this->hourly_tasks[] = $task;
        return $this;
    }
    
    
    /**
     * Add a task to be run every week.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */    
    public function addWeeklyTask($task) {
        $this->weekly_tasks[] = $task;
        return $this;
    }
    
    
    /**
     * Add a task to be run every month.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */    
    public function addMonthlyTask($task) {
        $this->monthly_tasks[] = $task;
        return $this;
    }
        
    
    /**
     * Add a task to be run every time the object is created.
     * 
     * @param string $task this can be a command or a path to an executable file
     * @return \Snap\TaskScheduler\Task
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */    
    public function addStartUpTask($task) {
        $this->start_up_tasks[] = $task;
        return $this;
    }
    
    
    /**
     * Start an infinite loop that runs the tasks.
     * 
     * @version 0.1.0 10/20/2016
     * @since 0.1.0 06/15/2016
     */
    public function run() {
        // start an infinite loop
        while($this->loop) {
            // update the flag file
            $this->setFlagFile();
            // get the current time
            $current_time = time();
            // run the tasks
            $this->runTasks($current_time);
            // pause
            sleep(30);
            // check for a halt flag
            $this->checkForHaltFlag();
        }
    }
    
    
    /**
     * Build an array of all tasks that need to be run and spawn them.
     * 
     * @param int $current_time current unix timestamp
     * @return void
     * @version 0.1.0 10/20/2016
     * @since 0.1.0 06/15/2016
     */
    protected function runTasks($current_time) {
        $tasks = [];
        // every minute tasks
        if($current_time - $this->every_minute_task_last_run_time > 60) {
            $this->every_minute_task_last_run_time = $current_time;
            if($this->every_minute_tasks) {
                $tasks = array_merge($tasks, $this->every_minute_tasks);
            }    
        }
        // five minute tasks
        if($current_time - $this->five_minute_task_last_run_time > 300) {
            $this->five_minute_task_last_run_time = $current_time;
            if($this->five_minute_tasks) {
                $tasks = array_merge($tasks, $this->five_minute_tasks);
            }
            // save the data to file every five minutes
            $this->saveDataToFile();
        }
        // ten minute tasks
        if($current_time - $this->ten_minute_task_last_run_time > 600) {
            $this->ten_minute_task_last_run_time = $current_time;
            if($this->ten_minute_tasks) {
                $tasks = array_merge($tasks, $this->ten_minute_tasks);
            }
        }
        // fifteen minute tasks
        if($current_time - $this->fifteen_minute_task_last_run_time > 900) {
            $this->fifteen_minute_task_last_run_time = $current_time;
            if($this->fifteen_minute_tasks) {
                $tasks = array_merge($tasks, $this->fifteen_minute_tasks);
            }
        }
        // thirty minute tasks
        if($current_time - $this->thirty_minute_task_last_run_time > 1800) {
            $this->thirty_minute_task_last_run_time = $current_time;
            if($this->thirty_minute_tasks) {
                $tasks = array_merge($tasks, $this->thirty_minute_tasks);
            }
        }
        // hourly tasks
        if($current_time - $this->hourly_task_last_run_time > 3600) {
            $this->hourly_task_last_run_time = $current_time;
            if($this->hourly_tasks) {
                $tasks = array_merge($tasks, $this->hourly_tasks);
            }
        }
        // daily tasks
        if($current_time - $this->daily_task_last_run_time > 86400) {
            $this->daily_task_last_run_time = $current_time;
            if($this->daily_tasks) {
                $tasks = array_merge($tasks, $this->daily_tasks);
            }
        }
        // weekly tasks
        if($current_time - $this->weekly_task_last_run_time > 604800) {
            $this->weekly_task_last_run_time = $current_time;
            if($this->weekly_tasks) {
                $tasks = array_merge($tasks, $this->weekly_tasks);
            }
        }
        // monthly tasks
        if($current_time - $this->monthly_task_last_run_time > (86400 * 30)) {
            $this->monthly_task_last_run_time = $current_time;
            if($this->monthly_tasks) {
                $tasks = array_merge($tasks, $this->monthly_tasks);
            }
        }
        // there are no tasks
        if(!$tasks) {
            $this->log($current_time . ' There are no tasks to run.' . PHP_EOL);
            return;
        }
        // execute the tasks
        $log = $current_time . ' Beginning to spawn tasks from process id ' . $this->process_id . ':' . PHP_EOL;
        foreach($tasks as $task) {
            $log .= 'Spawning task: ' . $task . PHP_EOL;
            // execute the task as a separate process
            exec($task);
        }
        // save log
        $this->log($log);        
    }
    
    
    /**
     * Set the data file path.
     * 
     * @param string $file_path
     * @return \Snap\Scheduler\Scheduler
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */
    public function setDataFilePath($file_path) {
        $this->path_to_data_file = (string) $file_path;
        return $this;
    }
    
    
    /**
     * Return the data file path.
     * 
     * @return string
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */
    public function getDataFilePath() {
        return (string) $this->path_to_data_file;
    }
    

    /**
     * Set the log directory.
     * 
     * @param string $directory_path
     * @return \Snap\Scheduler\Scheduler
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */
    public function setLogDirectory($directory_path) {
        // if the directory doesn't exist...
        if(!is_dir($directory_path)) {
            // create the directory and any needed recursive directories
            mkdir($directory_path, 0777, true);
        }
        $this->path_to_log_directory = (string) $directory_path;
        return $this;
    }
    
    
    /**
     * Return the log directory.
     * 
     * @return string
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */
    public function getLogDirectory() {
        return (string) $this->path_to_log_directory;
    }
    
    
    /**
     * Save data to file.
     * 
     * @return true on success
     * @throws \ErrorException if the data fails to write
     * @version 0.1.0 10/20/2016
     * @since 0.1.0 06/15/2016
     */
    protected function saveDataToFile() {
        // compile the data
        $data = [
            'daily_task_last_run_time' => (int) $this->daily_task_last_run_time,
            'hourly_task_last_run_time' => (int) $this->hourly_task_last_run_time,
            'thirty_minute_task_last_run_time' => (int) $this->thirty_minute_task_last_run_time,
            'fifteen_minute_task_last_run_time' => (int) $this->fifteen_minute_task_last_run_time,
            'ten_minute_task_last_run_time' => (int) $this->ten_minute_task_last_run_time,
            'five_minute_task_last_run_time' => (int) $this->five_minute_task_last_run_time,
            'every_minute_task_last_run_time' => (int) $this->every_minute_task_last_run_time,
        ];
        // convert the data to json
        $json = json_encode($data);
        // write to file
        if(!file_put_contents($this->path_to_data_file, $json)) {
            throw new \ErrorException('Failed to write data to file: ' 
                    . $this->path_to_data_file);
        }
        return true;
    }
    
    
    /**
     * Load the data file and import it into the object properties.
     * 
     * @param string $file_path
     * @return \Snap\Scheduler\Scheduler
     * @throws \ErrorException
     * @version 0.1.0 10/19/2016
     * @since 0.1.0 06/15/2016
     */
    protected function loadDataFromFile($file_path) {
        if(!is_readable($file_path)) {
            throw new \ErrorException('Data file (' . $file_path 
                    . ') does not exist or is not readable.');
        }
        // get the data array
        $data_array = (array) json_decode(include($file_path), true);
        // import the data
        $this->importData($data_array);
        return $this;
    }
    
    
    /**
     * Import the data array into the object properties.
     * 
     * @param array $data
     * @return \Snap\Scheduler\Scheduler
     * @version 0.1.0 06/15/2016
     * @since 0.1.0 06/15/2016
     */
    protected function importData(array $data) {
        // load the data
        if(isset($data['daily_task_last_run_time'])) {
            $this->daily_task_last_run_time = (int) $data['daily_task_last_run_time'];
        }
        if(isset($data['hourly_task_last_run_time'])) {
            $this->hourly_task_last_run_time = (int) $data['hourly_task_last_run_time'];
        }
        if(isset($data['thirty_minute_task_last_run_time'])) {
            $this->thirty_minute_task_last_run_time = (int) $data['thirty_minute_task_last_run_time'];
        }
        if(isset($data['fifteen_minute_task_last_run_time'])) {
            $this->fifteen_minute_task_last_run_time = (int) $data['fifteen_minute_task_last_run_time'];
        }
        if(isset($data['ten_minute_task_last_run_time'])) {
            $this->ten_minute_task_last_run_time = (int) $data['ten_minute_task_last_run_time'];
        }
        if(isset($data['five_minute_task_last_run_time'])) {
            $this->five_minute_task_last_run_time = (int) $data['five_minute_task_last_run_time'];
        }
        if(isset($data['every_minute_task_last_run_time'])) {
            $this->every_minute_task_last_run_time = (int) $data['every_minute_task_last_run_time'];
        }
        return $this;
    }
    
    
    /**
     * Save a log message.
     * 
     * @param string $log_msg
     * @return \Snap\Scheduler\Scheduler
     * @version 0.1.0 10/20/2016
     * @since 0.1.0 06/15/2016
     */
    protected function log($log_msg) {
        // create a file name
        $file_name = 'snap_task_scheduler_log_' . date('Y-m-d') . '.log';
        file_put_contents($this->getLogDirectory() . '/' . $file_name, $log_msg, FILE_APPEND);
        return $this;
    }
    
    
    /**
     * Set the flag file with the last time run.
     * 
     * @version 0.1.0 10/20/2016
     * @since 0.1.0 10/17/2016
     */
    protected function setFlagFile() {
        $flag_path = $this->directory_path . '/snap_scheduler_flag';
        if(file_put_contents($flag_path, time())) {
            return true;
        }
        throw new \ErrorException('Failed to set flag file.');
    }

    
    /**
     * Check for the existence of a halt flag file, and if it does exist, exit 
     *  the infinite loop.
     * 
     * @version 0.1.0 10/20/2016
     * @since 0.1.0 10/20/2016
     */
    protected function checkForHaltFlag() {
        if(file_exists($this->path_to_halt_flag)) {
            $this->loop = false;
            $this->log('Halt flag file found at (' . $this->path_to_halt_flag 
                    . ') for process id: ' . $this->process_id 
                    . '. Halting loop.' . PHP_EOL);
        }
    }
    
    
    /**
     * Destructor - log and attempt to respawn an instance.
     *
     * @version 0.1.0 10/20/2016
     * @since 0.1.0 06/15/2016
     */
    public function __destruct() {
        // log
        $this->log('--- ' . time() . ' Object (' . __CLASS__ . ' - Process ID: ' . $this->process_id 
                . ') destructor called from file (' . $calling_file_path . '). ---' . PHP_EOL);
        // save the data to file
        $this->saveDataToFile();
        // check for the halt flag file
        if(file_exists($this->path_to_halt_flag)) {
            // halt flag exists - delete it
            unlink($this->path_to_halt_flag);
        }
        else {
            // spawn a new instance of the scheduler
            exec($this->calling_file_path);
        }
    }
    
    
    /**
     * See if the scheduler is running or not.
     * 
     * @return boolean
     * @version 0.1.0 10/20/2016
     * @since 0.1.0 10/17/2016
     */
    public static function isRunning() {
        // get the scheduler's directory
        $flag_path = __DIR__ . '/snap_scheduler/snap_scheduler_flag';
        // flag file does not exist
        if(!file_exists($flag_path)) {
            return false;
        }
        // load the flag file's timestamp
        $last_run_time = file_get_contents($flag_path);
        // check if the timestamp is too old (10 min)
        if(!$last_run_time || (time() - $last_run_time) > 600) {
            return false;
        }
        // scheduler is running
        return true;
    }
    
    
}
