<?php
/*
 * Abstract file to make writing interactive cli applications a snap!
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2016, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\Cli
 * @version 10/20/2016 0.1.0
 * @since 10/10/2016  0.1.0
 * @license MIT - https://opensource.org/licenses/MIT
 */
namespace Snap\Cli;


// declare constants
if(!defined('EOL')) {
    define('EOL', PHP_EOL);
}
if(!defined('ESC')) {
    define('ESC', "\033");
}
if(!defined('CLEAR')) {
    define('CLEAR', ESC . "[2J");
}
if(!defined('HOME')) {
    define('HOME', ESC . "[0;0f");
}


abstract class SnapCliAbstract {
    
    /**
     *
     * @var string the title of the app (set in extending class)
     */
    protected $app_title = '';
    
    /**
     *
     * @var string the version of the app (set in extending class)
     */
    protected $app_version = '';
    
    /**
     *
     * @var array override this with your menu options 
     * The keys are the menu names and the values are the names of the methods
     */
    protected $menu = [];
    
    /**
     *
     * @var array holder for argv values after standardization
     */
    protected $arguments = [];
    
    /**
     *
     * @var array holder for argv options
     */
    protected $options = [];
    
    /**
     *
     * @var string 'windows' or 'posix'
     */
    protected $host_operating_system;
    
    /**
     *
     * @var string host operating system's user home directory
     */
    protected $home_dir_path;
    
    /**
     *
     * @var bool while true, the loop will run
     */
    protected $loop = true;
    
    
    
    public function __construct($path_to_home_directory = null) {
        // set the home directory and os
		if($path_to_home_directory) { 
		    $this->home_dir_path = (string) $path_to_home_directory;
	    }
        $this->getOsSettings();
        // standardize and set any arguments
        $this->standardizeAndSetArguments();
        // set any options passed on command line
        $this->setOptionsFromInput();
        // check for arguments
        if($this->getNumberOfArguments() > 0) {
            // there is an argument - try to call it as a method
            $this->callMethod($this->arguments[0], false);
        }
        else {
            // no arguments - start the loop
            $this->loop();
        }
    }
    
    
    protected function loop() {
        try {
            while($this->loop) {
                $this->clearScreen();
                $this->out($this->buildMenu());
                $input = $this->prompt('Please enter your choice:');
                $this->processMenuInput($input);
            }
        // catch any unhandled exceptions
        } catch (\Exception $ex) {
            $this->errorMsg($ex->getMessage() . EOL . 'File: ' 
                    . $ex->getFile() . ' Line: ' . $ex->getLine() . EOL);
            exit(1);
        }
        
    }
    
    
    protected function processMenuInput($input) {
        // quit
        if($input === 'q') {
            $this->loop = false;
            $this->out(EOL);
            exit;
        }
        // about
        elseif($input === 'a') {
            $this->callMethod('about', true);
        }
        // help
        elseif($input === 'h') {
            if($this->callMethod('help', false) === false) {
                $this->generateHelpPage();
            }
        }
        // look up menu item by number
        else {
            // cast input to an integer
            $int_input = (int) $input;
            if($int_input < 1) {
                throw new \InvalidArgumentException('Unsupported input option: ' 
                        . $input);
            }
            // valid integer - get the cooresponding menu name
            $menu_name = $this->getMenuNameByNumber($int_input);
            // call the method for this menu name
            $this->callMethod($this->menu[$menu_name], true);
        }
    }
    
    
    protected function getMenuNameByNumber($int) {
        $menu_keys = array_keys($this->menu);
        if(isset($menu_keys[($int - 1)])) {
            return (string) $menu_keys[($int - 1)];
        }
        throw new \OutOfRangeException('There is no menu name set for number: ' . $int);
    }
    
    
    protected function callMethod($method_name, $error_if_method_does_not_exist) {
        if(method_exists(get_called_class(), $method_name)) {
            // method exists - call it
            $this->{$method_name}();
        }
        else {
            // method does not exist
            if($error_if_method_does_not_exist) {
                throw new \BadMethodCallException('Method (' . $method_name 
                        . ') does not exist or is not callable from class: ' 
                        . get_called_class());
            }
            return false;
        }
    }
    
    protected function standardizeAndSetArguments() {
        if(!isset($argv) || empty($argv)) {
            return;
        }
        // remove php if it is the first argument
        if($argv[0] === 'php') {
            array_shift($argv);
        }
        // remove the script name (always there)
        array_shift($argv);
        // set as object property
        $this->arguments = $argv;        
    }
    
    protected function getOsSettings() {
        // windows
        if(stripos(php_uname('a'), 'win') !== false) {
            $this->host_operating_system = 'windows';
			// note: path to home drive must be set manually by user
			if(!$this->home_dir_path) {
			    exit('You need to set the path of your home (user) ' 
				. 'directory in the calling script so it is passed to the constructor.');
			}
        }
        // posix (linux, bsd, mac, etc) - assuming it is not windows....
        else {
            $this->host_operating_system = 'posix';
            exec('cd ~;pwd;', $output);
			$this->home_dir_path = $output[0];
        }
        // make sure the path exists and is a directory
        if(!is_dir($this->home_dir_path)) {
            throw new \ErrorException('Failed to find user home directory on '
                    . 'operating system. Attempted (' . $this->home_dir_path . ').');
        }
    }
    
    protected function setOptionsFromInput() {
        //$this->options = getopt();
    }
    
    protected function getNumberOfArguments() {
        return (int) count($this->arguments);
    }
    
    protected function argc() {
        return $this->getNumberOfArguments();
    }
    
    
    protected function buildMenu() {
        // no menu to build
        if(!isset($this->menu) || !$this->menu) {
            return;
        }
        // menu header
        if ($this->app_title) {
            $menu = '========================= ' . $this->app_title;
            if($this->app_version) {
                $menu .= ' ' . $this->app_version;
            }
            $menu .= ' =========================';
        }
        else {
            $menu = '============================================================';
        }
        $title_length = strlen($menu);
        $menu .= EOL . EOL;
        // build list
        $i = 1;
        // loop through the menu names
        $menu_names = (array_keys($this->menu));
        foreach($menu_names as $name) {
            $menu .= '    ' . $i . ' - ' . $name . EOL;
            $i++;
        }
        // help menu
        if(method_exists($this, 'help')) {
            $menu .= '    h - Help' . EOL;
        }
        // about menu - a good place for copyright, license, contact info, etc.
        if(method_exists($this, 'about')) {
            $menu .= '    a - About' . EOL;
        }
        // quit
        $menu .= '    q - Quit' . EOL;
        // footer
        $menu .= EOL;
        for($i=0; $i<$title_length; $i++) {
            $menu .= '=';
        }
        $menu .= EOL;

        return $menu;
    }

    
    public function clearScreen() {
        $this->out(CLEAR . HOME);
    }
    
    public function prompt($message, $default_value = 'required') {
        $this->out(EOL . trim($message) . ' ');
        $input = $this->getInput();
        // required input
        if($default_value === 'required' && ($input === '' || $input === null)) {
            while($input === '' || $input === null) {
                $this->out(EOL . 'Required input: ' . trim($message) . ' ');
                $input = $this->getInput();
            }
        } 
        if($input === '' || $input === null) {
            $input = $default_value;
        }
        return $input;
    }
    
    public function successMsg($message) {
        $this->out(EOL . ESC . "[0;32m" . trim($message) . ESC . "[0m" . EOL);
    }
    
    public function errorMsg($message) {
        fwrite(STDERR, EOL . ESC . "[41m" . 'Error: ' . trim($message) 
                . ESC . "[0m" . EOL);
    }
    
    public function getInput() {
        return (string) trim(fgets(STDIN));
    }
    
    public function out($text) {
        fwrite(STDOUT, $text);
    }
    
    public function cls() {
        $this->clearScreen();
    }
    
    public function promptPressEnterToContinue() {
        $this->prompt('Press the enter/return key to continue...', null);
    }
    
    public function echoArray(array $output) {
        $out = '';
        foreach($output as $line) {
            $out .= $line . EOL;
        }
        $out .= EOL;
        $this->out($out);
    }
    
    protected function generateHelpPage() {
        //TODO
        echo 'Generic help page (not yet implemented)' . EOL . EOL;
        $this->promptPressEnterToContinue();
    }
    
    public function getTerminalSize() {
        return [
            'rows' => intval(`tput lines`),
            'cols' => intval(`tput cols`),
        ];
    }
    
    protected function displayHeader($text, $clear_screen_first = true) {
        if($clear_screen_first) {
            $this->clearScreen();
        }
        $this->out('---------- ' . $text . ' ----------' . EOL . EOL);
    }
    
    
    public function getInfo() {
        return $this->app_title . ' ' . $this->app_version;
    }
    
}
